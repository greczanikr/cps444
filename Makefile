#*******************************************************************************
#
#      filename:  Makefile
#
#   description:  Implements a filter script.
#
#        author:  Greczanik, Bekah
#      login id:  SP_19_CPS444_25
#      
#         class:  CPS 444
#    instructor:  Perugini
#    assignment:  Homework #1
#
#      assigned:  February 18, 2019
#           due:  February 27, 2019
#           
#*******************************************************************************
CC = gcc
C_FLAGS = -c
PGM = keeplog
PGM2 = keeplog_helper
LIB = listlib

all: $(PGM)

$(PGM): $(PGM).o 
	$(CC) -o $(PGM) $(PGM).o -L. -lkeeplog_helper1 -lkeeplog_helper2 -llist

$(OBJECTS): $(PGM).c
	$(CC) $(C_FLAGS) $(PGM).c -I.

clean: 
	rm *.o $(PGM)  
